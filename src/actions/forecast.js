/*TODO*/
export const updateForecast = (forecast) => {
  return {
    type: "UPDATE_FORECAST",
    value: forecast,
  };
};

export const toggleLoader = (status) => {
  return {
    type: "TOGGLE_LOADER",
    status
  };
};

export const fetchForecast = (city) => {
  return async (dispatch) => {
    dispatch(toggleLoader(true));
    const reponse = await fetch(
      ` http://api.weatherstack.com/current?access_key=5590767b3a2f247afd6759f449254016&query=${city}`
    );
    const data = await reponse.json();
    dispatch(updateForecast(data));
    dispatch(toggleLoader(false));
  };
};
