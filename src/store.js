import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { forecast } from "./reducers/forecast";

const store = createStore(
  forecast,
  composeWithDevTools(applyMiddleware(thunk))
);

export default store;
