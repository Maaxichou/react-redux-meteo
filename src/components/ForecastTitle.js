import React, { Component } from "react";
import ForecastChoseCity from "./ForecastChoseCity";

class ForecastTitle extends Component {
  render() {
    return (
      <div className="location">
        <h1 className="city">{this.props.city}</h1>
        <h1 className="country">{this.props.country}</h1>
        <h1 className="date">{this.props.date}</h1>
        <ForecastChoseCity />
      </div>
    );
  }
}

export default ForecastTitle;
