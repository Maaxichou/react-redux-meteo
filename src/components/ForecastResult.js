import React, { Component } from "react";

class ForecastResult extends Component {
  render() {
    return (
      <div>
        <div className="meteo">
          <img alt="" className="logoWeather" src={this.props.img} />
          <h1 className="meteoComment">{this.props.comment}</h1>
          <h1 className="temperature">{this.props.temperature}°C</h1>
          <div className="minMax">
            <h1 className="temp">{this.props.min} </h1>
            <div className="separator" />
            <h1 className="temp">{this.props.max}</h1>
          </div>
          <div className="windHumidity">
            <div className="windLogo" />
            <h1 className="wind">{this.props.wind}km/h</h1>
            <div className="humidityLogo1" />
            <div className="humidityLogo2" />
            <h1 className="humidity">{this.props.humidity}%</h1>
          </div>
        </div>
      </div>
    );
  }
}

export default ForecastResult;
