let initialState = {
    forecast: {},
  	loader: false
};

export const forecast = (state = initialState, action) => {
  switch (action.type) {
      case 'UPDATE_FORECAST':
          return {
            ...state,
            forecast: action.value
          };
      case 'UPDATE_LOADER':
          return {
            ...state,
            loader: action.status
          };
      default :
          return state;
  }
};
